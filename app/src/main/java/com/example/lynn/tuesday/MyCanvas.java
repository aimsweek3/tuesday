package com.example.lynn.tuesday;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.RelativeLayout;

import static com.example.lynn.tuesday.MainActivity.*;

/**
 * Created by lynn on 6/20/2016.
 */
public class MyCanvas extends View {
    public Paint paint;
    public RelativeLayout.LayoutParams layoutParams;

    public MyCanvas(Context context) {
        super(context);

        paint = new Paint();

        paint.setColor(0xFFFF0000);

        layoutParams = (RelativeLayout.LayoutParams)getLayoutParams();
    }

    public void refresh() {
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        int theAlpha = Integer.parseInt(alpha.getSelectedItem().toString());
        int theRed = Integer.parseInt(red.getSelectedItem().toString());
        int theGreen = Integer.parseInt(green.getSelectedItem().toString());
        int theBlue = Integer.parseInt(blue.getSelectedItem().toString());

        paint.setColor(Color.argb(theAlpha,theRed,theGreen,theBlue));

        canvas.drawLine(0,0,500,500,paint);
    }
}
