package com.example.lynn.tuesday;

import android.view.View;

import static com.example.lynn.tuesday.MainActivity.*;

/**
 * Created by lynn on 6/20/2016.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
       myCanvas.refresh();
    }

}
