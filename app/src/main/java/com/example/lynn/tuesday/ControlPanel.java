package com.example.lynn.tuesday;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.example.lynn.tuesday.MainActivity.*;

/**
 * Created by lynn on 6/20/2016.
 */
public class ControlPanel extends LinearLayout {

    public TextView createTextView(Context context,
                                   String text) {
        TextView view = new TextView(context);

        view.setText(text);

        return(view);
    }

    public EditText createEditText(Context context) {
        EditText editText = new EditText(context);

        return(editText);
    }

    public ControlPanel(Context context) {
        super(context);

        button = new Button(context);

        button.setText("Press Me");

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200,200);

        button.setLayoutParams(layoutParams);

        button.setBackground(getResources().getDrawable(R.drawable.circle));

        button.setOnClickListener(listener);

        addView(button);

        List<Integer> list = new ArrayList<>();

        for (int counter=0;counter<256;counter++)
            list.add(counter);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(context,android.R.layout.simple_spinner_dropdown_item,list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        alpha = new Spinner(context);

        red = new Spinner(context);

        green = new Spinner(context);

        blue = new Spinner(context);

        alpha.setAdapter(adapter);
        red.setAdapter(adapter);
        green.setAdapter(adapter);
        blue.setAdapter(adapter);

        addView(createTextView(context,"Alpha"));
        addView(alpha);
        addView(createTextView(context,"Red"));
        addView(red);
        addView(createTextView(context,"Green"));
        addView(green);
        addView(createTextView(context,"BLue"));
        addView(blue);

        button.setOnClickListener(listener);
    }

}
