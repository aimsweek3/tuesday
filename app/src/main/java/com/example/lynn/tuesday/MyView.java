package com.example.lynn.tuesday;

import android.content.Context;
import android.widget.TableLayout;
import android.widget.TableRow;

import static com.example.lynn.tuesday.MainActivity.*;

/**
 * Created by lynn on 6/20/2016.
 */
public class MyView extends TableLayout {

    public MyView(Context context) {
        super(context);

        TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.MATCH_PARENT);

        setLayoutParams(layoutParams);

        setStretchAllColumns(true);

        TableRow row1 = new TableRow(context);

        row1.addView(new ControlPanel(context));

        TableRow row2 = new TableRow(context);

        row2.addView(new OutputPanel(context));

        addView(row1);
        addView(row2);


    }

}
