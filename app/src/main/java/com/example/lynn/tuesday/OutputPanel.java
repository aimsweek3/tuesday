package com.example.lynn.tuesday;

import android.content.Context;
import android.widget.RelativeLayout;

import static com.example.lynn.tuesday.MainActivity.*;

/**
 * Created by lynn on 6/20/2016.
 */
public class OutputPanel extends RelativeLayout {

    public OutputPanel(Context context) {
        super(context);

        myCanvas = new MyCanvas(context);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(500,500);

        myCanvas.setLayoutParams(layoutParams);

        addView(myCanvas);
    }

}
